<?

use Bitrix\Iblock\Component\Tools;
use Bitrix\Main\Loader;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();


class DolgushinUsersComponent extends \CBitrixComponent
{

    public function onPrepareComponentParams($arParams)
    {
        return $arParams;
    }

    public function executeComponent()
    {
        if (!empty($_REQUEST['save'])) {
            $obUser = new CUser();
            $obUser->Update(
                $_REQUEST['id'],
                [
                    'LOGIN' => $_REQUEST['login'],
                    'NAME' => $_REQUEST['name'],
                    'LAST_NAME' => $_REQUEST['surname'],
                    'EMAIL' => $_REQUEST['email']
                ]
            );
        }

        $arOrder = array('sort' => 'asc');
        $sSort = 'sort';
        $rsUsers = CUser::GetList($arOrder, $sSort, ['>ID' => 2]);

        while ($user = $rsUsers->Fetch()) {
            $this->arResult['USERS'][] = $user;
        }

        $this->includeComponentTemplate();
    }


}