<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = [
    'NAME' => 'Список пользователей',
    'DESCRIPTION' => 'Список пользователей',
    'ICON' => '/images/icon.gif',
    'CACHE_PATH' => 'Y',
    'PATH' => [
        'ID' => 'dolgushin',
        'NAME' => 'dolgushin',
    ],
];