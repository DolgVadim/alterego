<?php
/** @var CBitrixComponent $component */
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */

$this->setFrameMode(true);
?>


<div class="container">
    <div class="row justify-content-center">
        <? foreach ($arResult['USERS'] as $arUser): ?>
            <div class="col-md-6 mb-3">
                <div class="card">
                    <div class="card-header"><?=$arUser['NAME']?></div>
                    <div class="card-body">
                        <form method="POST" action=''>
                            <?=bitrix_sessid_post();?>
                            <input type="hidden" name="save" value="y">
                            <input type="hidden" name="id" value="<?=$arUser['ID']?>">

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Логин</label>

                                <div class="col-md-6">
                                    <input id="login" type="text"
                                           class="form-control"
                                           name="login" value="<?= $arUser['LOGIN']; ?>" required autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Имя</label>

                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control"
                                           name="name"
                                           value="<?= $arUser['NAME']; ?>" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Фамилия</label>

                                <div class="col-md-6">
                                    <input id="surname" type="text"
                                           class="form-control"
                                           name="surname" value="<?= $arUser['LAST_NAME']; ?>" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">E-mail</label>

                                <div class="col-md-6">
                                    <input id="email" type="email"
                                           class="form-control"
                                           name="email" value="<?= $arUser['EMAIL']; ?>" required>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Сохранить
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    </div>
</div>