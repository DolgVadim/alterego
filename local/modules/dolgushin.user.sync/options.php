<?
$module_id = "dolgushin.user.sync";

CModule::IncludeModule('dolgushin.user.sync');

$arAllOptions = array(
    array('CODE' => 'sync_api_uri', 'NAME' => 'Ссылка на апи', 'VALUE' => 'http://dolgushin.tk/api/', 'INPUT' => Array('text', 20)),
    array('CODE' => 'sync_api_token', 'NAME' => 'Токен', 'VALUE' => '123', 'INPUT' => Array('text', 10)),
);


if (!empty($_REQUEST['UPDATE']) && check_bitrix_sessid()) {

    foreach ($arAllOptions as $option) {
        $code = $option['CODE'];
        $val = htmlspecialchars($_REQUEST[$code]);
        if (!empty($val))
            COption::SetOptionString($module_id, $code, $val);
    }
}

$aTabs = array(
    array('DIV' => 'edit1', 'TAB' => 'Настройки', 'ICON' => '', 'TITLE' => 'Настройки'),
);

$tabControl = new CAdminTabControl('tabControl', $aTabs);
?>
<?
$tabControl->Begin();
?>
<form method="POST" action="<?= $APPLICATION->GetCurPage() ?>?lang=ru&mid=<?= $module_id ?>">
    <?
    echo bitrix_sessid_post();
    $tabControl->BeginNextTab();
    foreach ($arAllOptions as $Option) {

        $val = COption::GetOptionString($module_id, $Option['CODE']);
        $type = $Option['INPUT'];
        ?>
        <tr>
            <td valign="top" width="50%"><?
                if ($type[0] == "checkbox")
                    echo "<label for=\"" . htmlspecialcharsbx($Option['CODE']) . "\">" . $Option['NAME'] . "</label>";
                else
                    echo $Option['NAME'];
                ?></td>
            <td valign="middle" width="50%">
                <?
                if ($type[0] == "checkbox"):?>
                    <input type="checkbox" name="<?
                    echo htmlspecialcharsbx($Option['CODE']) ?>" id="<?
                    echo htmlspecialcharsbx($Option['CODE']) ?>" value="Y"<?
                    if ($val == "Y") echo " checked"; ?>>
                <? elseif ($type[0] == "text"): ?>
                    <input type="text" size="<?
                    echo $type[1] ?>" value="<?
                    echo htmlspecialcharsbx($val) ?>" name="<?
                    echo htmlspecialcharsbx($Option['CODE']) ?>">
                <? elseif ($type[0] == "textarea"): ?>
                    <textarea rows="<?
                    echo $type[1] ?>" cols="<?
                    echo $type[2] ?>" name="<?
                    echo htmlspecialcharsbx($Option['CODE']) ?>"><?
                        echo htmlspecialcharsbx($val) ?></textarea>
                <? endif ?>
            </td>
        </tr>
        <?
    }
    ?>

    <? $tabControl->Buttons(); ?>

    <input type="submit" name="UPDATE" value="<? echo GetMessage("MAIN_SAVE") ?>">
    <input type="hidden" name="UPDATE" value="Y">
    <? $tabControl->End(); ?>
</form>
