<?

Class dolgushin_user_sync extends CModule
{
    var $MODULE_ID = "dolgushin.user.sync";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME = "Синхронизация пользователей";
    var $MODULE_DESCRIPTION = "Синхронизация пользователей";
    var $MODULE_CSS;
    var $MODULE_GROUP_RIGHTS = "Y";

    function dolgushin_user_sync()
    {
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path . "/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        } else {
            $this->MODULE_VERSION = 1;
            $this->MODULE_VERSION_DATE = 1;
        }

        $this->PARTNER_NAME = "Долгушин Вадим";

    }

    function InstallFiles()
    {
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/local/modules/dolgushin.user.sync/user.sync/", $_SERVER["DOCUMENT_ROOT"]."/api/user.sync/", true, true);

        return true;
    }

    function InstallEvents()
    {
        RegisterModuleDependences(
            'main',
            'OnAfterUserAdd',
            $this->MODULE_ID,
            'Dolgushin\User\Sync\Handler',
            'userSyncAdd'
        );


        RegisterModuleDependences(
            'main',
            'OnAfterUserUpdate',
            $this->MODULE_ID,
            'Dolgushin\User\Sync\Handler',
            'userSyncUpdate'
        );

        return true;
    }

    function UnInstallEvents()
    {
        UnRegisterModuleDependences(
            'main',
            'OnAfterUserAdd',
            $this->MODULE_ID,
            'Dolgushin\User\Sync\Handler',
            'userSync'
        );

        UnRegisterModuleDependences(
            'main',
            'OnAfterUserUpdate',
            $this->MODULE_ID,
            'Dolgushin\User\Sync\Handler',
            'userSyncUpdate'
        );

        return true;
    }


    function DoInstall()
    {
        RegisterModule("dolgushin.user.sync");
        $this->InstallEvents();
        $this->InstallFiles();
    }

    function DoUninstall()
    {
        $this->UnInstallEvents();
        UnRegisterModule("dolgushin.user.sync");

    }
}

?>
