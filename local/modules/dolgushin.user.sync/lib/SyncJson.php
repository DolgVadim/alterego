<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 05.08.18
 * Time: 10:20
 */

namespace Dolgushin\User\Sync;

class SyncJson implements UserSyncInterface
{
    protected $sToken;
    protected $sServer;
    const MODULE_ID = 'dolgushin.user.sync';

    public function __construct()
    {
        $this->sToken = \COption::GetOptionString(self::MODULE_ID, 'sync_api_token');
        $this->sServer = \COption::GetOptionString(self::MODULE_ID, 'sync_api_uri');;
    }

    public function send($arData)
    {
        $sData = json_encode($arData);
        $ch = curl_init($this->sServer);
        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            [
                'Content-Type: application/json',
                'Sync-Type: json',
                'Content-Length: ' . strlen($sData),
                'Sync-Token: ' . $this->sToken
            ]
        );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $sData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $arResult = json_decode(curl_exec($ch),true);
        curl_close($ch);

        return $arResult;
    }

    public function receive()
    {
        return json_decode(file_get_contents('php://input'), true);
    }

    public function answer($status, $message)
    {
        return json_encode(['status' => $status, 'message' => $message]);
    }

}