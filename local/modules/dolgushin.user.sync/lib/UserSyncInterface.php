<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 05.08.18
 * Time: 10:15
 */

namespace Dolgushin\User\Sync;


interface UserSyncInterface
{
    public function send($arData);

    public function receive();

    public function answer($status, $message);

}