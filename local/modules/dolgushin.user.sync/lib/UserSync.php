<? require_once($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_before.php');

$module_id = 'dolgushin.user.sync';

\Bitrix\Main\Loader::includeModule($module_id);

$sApiKey = \COption::GetOptionString($module_id, 'sync_api_token');
$sUrl = \COption::GetOptionString($module_id, 'sync_api_uri');

$sSyncType = $_SERVER['HTTP_SYNC_TYPE'];

$obSync = \Dolgushin\User\Sync\UserSyncFactory::create($sSyncType);

$arData = $obSync->receive();

$arOrder = array('sort' => 'asc');
$sSort = 'sort';

if (empty($arData['id'])) {
    echo $obSync->answer('error', 'no user id');
    exit;
}

$arUser = CUser::GetList($arOrder, $sSort, ['UF_EXT_ID' => $arData['id']])->Fetch();

if ($arUser) {
    $obUser = new CUser;
    $obUser->Update(
        $arUser['ID'],
        [
            'NAME' => $arData['name'],
            'LAST_NAME' => $arData['surname'],
            'EMAIL' => $arData['email'],
            'LOGIN' => $arData['login'],
        ]
    );
    $sError = $obUser->LAST_ERROR;
    if (empty($sError)) {
        echo $obSync->answer('update', 'user updated');
        exit;
    }
} else {
    $obUser = new CUser;
    $ID = $obUser->Add(
        [
            'NAME' => $arData['name'],
            'LAST_NAME' => $arData['surname'],
            'EMAIL' => $arData['email'],
            'LOGIN' => $arData['login'],
            'UF_EXT_ID' => $arData['id'],
            "PASSWORD" => $arData['password']?:'123456',
            "CONFIRM_PASSWORD" => $arData['password']?:'123456',
        ]
    );
    $sError = $obUser->LAST_ERROR;
    if (empty($sError)) {
        echo $obSync->answer('added', $ID);
        exit;
    }
}

echo $obSync->answer('error', $sError);
