<?php

namespace Dolgushin\User\Sync;

class Handler
{
    public function userSyncAdd($arData)
    {
        if (!empty($arData['UF_EXT_ID']))
            return;

        $obSync = new SyncJson();
        $arUser = [
            'name' => $arData['NAME'],
            'surname' => $arData['LAST_NAME'],
            'email' => $arData['EMAIL'],
            'login' => $arData['LOGIN'],
            'id' => $arData['ID'],
            'password' => $arData['CONFIRM_PASSWORD'],
        ];

        $arResponse = $obSync->send($arUser);

        if ($arResponse['status'] == 'added') {
            $obUser = new \CUser;
            $obUser->Update(
                $arUser['id'],
                [
                    'UF_EXT_ID' => $arResponse['message'],
                ]
            );
        }
    }

    public function userSyncUpdate($arData)
    {

        $obSync = new SyncJson();
        $arUser = [
            'name' => $arData['NAME'],
            'surname' => $arData['LAST_NAME'],
            'email' => $arData['EMAIL'],
            'login' => $arData['LOGIN'],
            'id' => $arData['ID'],
        ];

        $obSync->send($arUser);
    }
}