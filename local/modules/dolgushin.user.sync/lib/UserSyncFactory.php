<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 13.08.18
 * Time: 23:12
 */

namespace Dolgushin\User\Sync;


final class UserSyncFactory
{
    public static function create(string $sSyncFormat): UserSyncInterface
    {
        $sClass = 'Dolgushin\\User\\Sync\\Sync' . ucwords(strtolower($sSyncFormat));
        return new $sClass();
    }
}